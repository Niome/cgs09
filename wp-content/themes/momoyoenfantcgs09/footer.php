<?php
/**
 * Footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package momoyo
 */
?>

		</div><!-- #content -->
	</div><!-- #page -->
</div><!-- container -->

<!-- <div id="footer"> 
	<p> Copyright &#169; 
		/*<?php print(date(Y)); ?>
		<?php bloginfo('name'); ?> <br /> Blog propulsé par 
		<a href="http://wordpress.org/">WordPress</a> et con&ccedil;u par 
		<a href="http://www.fran6art.com">Fran6art</a> <br /> 
		<a href="feed:<?php bloginfo('rss2_url'); ?>">Articles (RSS)</a> et 
		<a href="feed:<?php bloginfo('comments_rss2_url'); ?>">Commentaires (RSS)</a>. 
		<?php echo get_num_queries(); ?> requêtes. <?php timer_stop(1); ?> secondes. 
	</p> 
</div> -->

	<footer id="colophon" class="site-footer">
	<div class="container">

         <div class="site-info">
            <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'momoyo' ) ); ?>"><?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'La Cité du Goût et des Saveurs' ), 'WordPress' );
			?></a>
			<span class="sep"> | </span>
			<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'momoyo' ), 'Momoyo', 'Benachi' );
			?>
         </div><!-- .site-info -->

		</div>
	</footer><!-- #colophon -->



<?php wp_footer(); ?>

</body>
</html>
